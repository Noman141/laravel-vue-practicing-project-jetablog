<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('category',\App\Http\Controllers\Backend\CategoryController::class);
Route::delete('/categories/select/{id}',[\App\Http\Controllers\Backend\CategoryController::class, 'deleteSelected'])->name('category.delete.selected');

Route::resource('post',\App\Http\Controllers\Backend\PostController::class);


// frontend routes
Route::get('/public/posts',[\App\Http\Controllers\Frontend\PostController::class, 'index'])->name('post.index');
Route::get('/public/post/{id}',[\App\Http\Controllers\Frontend\PostController::class, 'view'])->name('post.view');


Route::get('/public/categories',[\App\Http\Controllers\Frontend\CategoryController::class, 'index'])->name('category.index');
Route::get('/public/categories/{id}/post',[\App\Http\Controllers\Frontend\CategoryController::class, 'postByCategory'])->name('category.post');

Route::get('/public/search',[\App\Http\Controllers\Frontend\PostController::class, 'search'])->name('post.search');


