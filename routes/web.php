<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Auth::routes();

Route::get('/admin/home', [\App\Http\Controllers\HomeController::class, 'index']);

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'home']);



Route::get('{path}', function () {
    $url = url()->current();
    $url_path = \Illuminate\Support\Str::after($url,'http://127.0.0.1:8000/');
    return view('frontend.index',['path' => $url_path]);

})->where('path', '(.*)');

