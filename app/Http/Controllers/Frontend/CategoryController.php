<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $categories = Category::with('posts')->get();
        return response()->json(['categories' => $categories], 200);
    }

    public function postByCategory($id){
        $category = Category::find($id);
        $categoryPosts = Post::where('category_id',$category->id)->get();
        return response()->json(['categoryPosts'=> $categoryPosts], 200);
    }
}
