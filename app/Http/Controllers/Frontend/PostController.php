<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\Post;
use Request;

class PostController extends Controller{

    public function index(){
        $posts = Post::with('category','user')->orderBy('id','desc')->get();
        return response()->json(['posts' => $posts], 200);
    }

    public function view($id){
        $post = Post::with('user','category')->where('id', $id)->first();
        return response()->json(['post' => $post]);
    }
    public function search(){
        $search = Request::get('s');
        $post = Post::with('user','category')
            ->where("title","LIKE","%{$search}%")
            ->orWhere("body","LIKE","%{$search}%")
            ->get();

        return response()->json(['post' => $post]);
    }
}
