<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $categories = Category::orderBy('id','desc')->get();
        return response()->json(['categories' => $categories],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $this->validate($request, [
           'name' => 'required|string|unique:categories',
        ]);

        $category = Category::create([
           'name' => $request->name,
           'slug' => Str::slug($request->name),
           'status'=> 0
        ]);

        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($slug){
        $category = Category::where('slug',$slug)->first();
        if ($category){
            return response()->json($category, 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $slug){
        $category = Category::where('slug',$slug)->first();

        $request->validate([
           'name' => "required|string|unique:categories,name, $category->id"
        ]);
        $category->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ]);

        return response()->json('success',200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $category = Category::find($id);
        $category->delete();
        return response()->json('success',200);
    }

    public function deleteSelected($ids){
        $selectedId = explode(',',$ids);
        foreach ($selectedId as $id){
            $category = Category::find($id);
            $posts = Post::orderBy('id','desc')->where('category_id',$category->id)->get();
            if ($posts){
                foreach ($posts as $post) {
                    if ($post->image) {
                        $imgPath = public_path() . '/images/backend/post/';
                        $img = $imgPath . $post->image;
                        if (file_exists($img)) {
                            @unlink($img);
                        }
                    }
                    $post->delete();
                }
            }
            $category->delete();
        }
        return response()->json('success',200);
    }
}
