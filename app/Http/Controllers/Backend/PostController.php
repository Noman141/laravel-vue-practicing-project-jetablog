<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use File;

class PostController extends Controller{
        public $image;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $posts = Post::with('category','user')->orderBy('id','desc')->get();

        return response()->json(['posts' => $posts],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $request->validate([
           'title' => "string|required",
           'category_id' => "numeric|required",
        ]);

        $image = $request->image;
        $extension = Str::between($image,'data:image/',';base64');
        $location = public_path().'/images/backend/post/';
        $imageName = Str::slug($request->title).'-'.time().'.'.$extension;
        Image::make($image)->save($location.$imageName);

        $post = new Post();

        $post->title = $request->title;
        $post->category_id = $request->category_id;
        $post->user_id = $request->user_id;
        $post->slug = Str::slug($request->title);
        $post->body = $request->body;
        $post->image = $imageName;

        $post->save();

        return response()->json('success',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id){
        $post = Post::find($id);

        return response()->json(['post' => $post], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'title' => "string|required",
            'category_id' => "numeric|required",
        ]);

        $post = Post::find($id);
//        delete old Image
        if ($request->image){
            $imgPath =  public_path().'/images/backend/post/';
            $img = $imgPath.$post->image;
            if(file_exists($img)){
                @unlink($img);
            }
        }
//      upload new Image
        if ($request->image){
            $image = $request->image;
            $extension = Str::between($image,'data:image/',';base64');
            $location = public_path().'/images/backend/post/';
            $imageName = Str::slug($request->title).'-'.time().'.'.$extension;
            Image::make($image)->save($location.$imageName);
            $post->image = $imageName;
        }

        $post->title = $request->title;
        $post->category_id = $request->category_id;
        $post->slug = Str::slug($request->title);
        $post->body = $request->body;

        $post->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $post = Post::find($id);

        if ($post->image){
           $imgPath =  public_path().'/images/backend/post/';
           $img = $imgPath.$post->image;
           if(file_exists($img)){
               @unlink($img);
           }
        }
        $post->delete();

        return response()->json('success',200);
    }
}
