
export default {
    state: {
        categories: [],
        posts: [],
        publicPosts: [],
        singlePost: [],
        publicCategories: [],
        publicCategoryPosts:[]
    },
    getters: {
        getCategories(state){
            return state.categories
        },
        getAllPosts(state){
            return state.posts
        },
        getAllPublicPosts(state){
            return state.publicPosts
        },
        getSinglePost(state){
            return state.singlePost
        },
        getPublicCategories(state){
            return state.publicCategories
        },
        getAllCategoryPosts(state){
            return state.publicCategoryPosts
        }

    },
    actions: {
        allCategory(context){
            axios.get('/api/category')
                 .then((response) => {
                     context.commit('getAllCategory', response.data.categories)
                 })
        },
        getAllPosts(context){
            axios.get('/api/post')
                .then((response) => {
                    // console.log(response.data.posts)
                    context.commit('getAllPosts', response.data.posts)
                })
        },
        getAllPublicPosts(context){
            axios.get('/api/public/posts')
                 .then((response) => {
                     context.commit('getAllPublicPosts', response.data.posts)
                 })
        },
        getSinglePost(context,payload){
            axios.get(`/api/public/post/${payload}`)
                .then((response) => {
                    context.commit('getSinglePost', response.data.post)
                })
        },
        getPublicCategories(context){
            axios.get('/api/public/categories')
                 .then((response) => {
                     context.commit('getPublicCategories', response.data.categories)
                 })
        },
        getAllCategoryPosts(context, payload){
            axios.get(`/api/public/categories/${payload}/post`)
                .then((response) => {
                    context.commit('getAllCategoryPosts', response.data.categoryPosts)
                })
        },
        searchPost(context, payload){
            axios.get(`/api/public/search?s=${payload}`)
                 .then((response) => {
                     context.commit('searchPost', response.data.post)
                 })
        }
    },
    mutations: {
        getAllCategory(state,data){
            return state.categories = data;
        },
        getAllPosts(state, data){
            return state.posts = data;
        },
        getAllPublicPosts(state, data){
            return state.publicPosts = data;
        },
        getSinglePost(state, data){
            return state.singlePost = data;
        },
        getPublicCategories(state, data){
            return state.publicCategories = data;
        },
        getAllCategoryPosts(state, data){
            return state.publicCategoryPosts = data;
        },
        searchPost(state, data){
             state.publicPosts = data;
        }
    }
}
