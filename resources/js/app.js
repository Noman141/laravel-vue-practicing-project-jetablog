require('./bootstrap');
// window.Vue = require('vue');
import Vue from 'vue';

import VueRouter from 'vue-router';
import { Form, HasError, AlertError } from 'vform';
import routes from './routes/index';
import Swal from 'sweetalert2';
import Vuex from 'vuex';
import storedata from './store/index';
import {filter} from './filter/index'

import 'v-markdown-editor/dist/v-markdown-editor.css';
import Editor from 'v-markdown-editor';
import moment from 'moment'



// global register
Vue.use(Editor);

Vue.use(Vuex);

const store = new Vuex.Store(
    storedata
)

window.Swal = Swal;
const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
window.Toast = Toast;

Vue.use(VueRouter);

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

const router = new VueRouter({
    mode: 'history',
    routes

})

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admin-master', require('./components/backend/layouts/master').default);
Vue.component('frontend', require('./components/frontend/layouts/master.vue').default);


const app = new Vue({
    el: '#app',
    router,
    store
});

