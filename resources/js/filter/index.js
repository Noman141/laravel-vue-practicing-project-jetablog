import Vue from 'vue'
import moment from 'moment'

Vue.filter('timeFormat', (arg) => {
    return moment(arg).format('MMM Do YYYY')
})


Vue.filter('postBodyShorter', (text,length, suffix) => {
    return text.substr(0, length) + suffix;
})
