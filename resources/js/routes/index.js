import AdminIndex from '../components/backend/index';
import EX from '../components/ExampleComponent';
import CategoryList from '../components/backend/category/index';
import CategoryCreate from '../components/backend/category/create';
import CategoryEdit from '../components/backend/category/edit';
import PostList from '../components/backend/post/index';
import PostCreate from '../components/backend/post/create';
import PostEdit from '../components/backend/post/edit';

// frontend components
import FrontendIndex from '../components/frontend/index';
import BlogPosts from '../components/frontend/pages/blogPosts.vue';
import SinglePost from '../components/frontend/pages/singlePost';
import sideBar from '../components/frontend/pages/sideBar.vue';
import CategoryPost from '../components/frontend/pages/categoryPost';

const routes = [
    {
        path: '/home',
        component: AdminIndex,
        name: 'admin-index'
    },
    {
        path: '/ex',
        component: EX,
        name: 'admin-ex'
    },
    {
        path: '/category-list',
        component: CategoryList,
        name: 'category-list'
    },
    {
        path: '/category-create',
        component: CategoryCreate,
        name: 'category-create'
    },
    {
        path: '/category-edit/:slug',
        component: CategoryEdit,
        name: 'category-edit'
    },
    {
        path: '/post-list',
        component: PostList,
        name: 'post-list'
    },
    {
        path: '/post-create',
        component: PostCreate,
        name: 'post-create'
    },
    {
        path: '/post-edit/:id',
        component: PostEdit,
        name: 'post-edit'
    },

    // backend Routes end here


    // frontend routes start from here

    {
        path: '/',
        component: FrontendIndex,
        name: 'frontend-index'
    },
    {
        path: '/posts',
        component: BlogPosts,
        name: 'frontend-posts'
    },
    {
        path: '/post/:id',
        component: SinglePost,
        name: 'frontend-post'
    },
    {
        path: '/sidebar',
        component: sideBar,
        name: 'frontend-sidebar'
    },
    {
        path: '/category/:id/post',
        component: CategoryPost,
        name: 'frontend-category-post'
    },

]
export default routes;
